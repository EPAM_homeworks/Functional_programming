def is_armstrong(n):
    string_num = list(str(n))
    length = len(string_num)
    sum_of_digits = sum(list(map(lambda x: x**length, list(map(int, string_num)))))
    if sum_of_digits == n:
        return True
    else:
        return False

is_armstrong(153)