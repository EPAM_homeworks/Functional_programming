def collatz_steps(n):
    if n == 1:
        return 0
    nxt = lambda x: x//2 if x%2 == 0 else x*3+1
    return 1+collatz_steps(nxt(n))

assert collatz_steps(16) == 4
steps = 0
assert collatz_steps(12) == 9
steps = 0
assert collatz_steps(1000000) == 152