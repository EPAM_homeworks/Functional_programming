from functools import reduce

# 1st way
def get_sum():
    cnt = 0
    for n in range(1000000):
        if n**2 > 1000000:
            break
        cnt += n**2
    return cnt

print(get_sum())

# 2d way
print(reduce(lambda a,b: a+b, [x**2 for x in range(1000)]))

# 3d way
print(sum([x**2 for x in range(1000)]))