import math 
from functools import reduce
# 6
diff = sum(range(101))**2 - sum([x**2 for x in range(101)])
print(diff)

# 9
triplet_mult = [a*b*(1000-a-b) for a in range(4, 999) for b in range(a, 999) if (math.sqrt(a**2+b**2)) == 1000-a-b][0]
print(triplet_mult)

# 40

a = ''.join(list(map(str, [i for i in range(1, 185186)])))
a = reduce(lambda x,y: x*y, list(map(int, [a[x] for x in [0, 9, 99, 999, 9999, 99999, 999999]])))
# OR a = reduce(lambda x, y: x * y, map(int, map(lambda x: ''.join(map(str, range(1, 4 ** 9)))[x-1], [1, 10, 100, 1000, 10000, 100000, 1000000])))
print(a)

# 48
print(str(sum([i**i for i in range(1, 1001)]))[-10:])